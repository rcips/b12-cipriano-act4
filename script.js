console.log("Hello World!")

let person = {
	firstName: "Rallion",
	lastName: "Cipriano",
	age: 30,
	hobbies: ["Basketball", "Coding", "Watching K-drama"],
	address: "Blk 31 lot 6",
	city: "Navotas",
	street: "Banak",
}

console.log(`First Name: ${person.firstName}`)
console.log(`Last Name: ${person.lastName}`)
console.log(`Age: ${person.age}`)
console.log(`Hobbies: ${person.hobbies}`)
console.log(`Address: ${person.address}  ${person.city}`)

function sentence(firstName, lastName, age) {
	console.log(`${firstName} ${lastName} is ${age} years of age`)
}

sentence("Rallion", "Cipriano", 30)

function hobbies(hobbies) {
	console.log(`His hobbies are : ${hobbies}`)
}

hobbies("Basketball, Coding, Watching K-drama")

let place = {
	address: "Blk 31 lot 6",
	city: "Navotas",
	street: "Banak",
	Married: true,
}

console.log(`Address: ${place.address}`)
console.log(`City :${place.city}`)
console.log(`Street:${place.street}`)
console.log(`The value of Married:${place.Married}`)
